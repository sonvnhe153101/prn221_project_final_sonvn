﻿using System;
using System.Collections.Generic;

namespace Final_Project_HE153101.Models
{
    public partial class Subject
    {
        public Subject()
        {
            Schedules = new HashSet<Schedule>();
        }

        public int SubjectId { get; set; }
        public string SubjectName { get; set; } = null!;

        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}
