﻿using System;
using System.Collections.Generic;

namespace Final_Project_HE153101.Models
{
    public partial class Teacher
    {
        public Teacher()
        {
            Schedules = new HashSet<Schedule>();
        }

        public int TeacherId { get; set; }
        public string TeacherName { get; set; } = null!;

        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}
