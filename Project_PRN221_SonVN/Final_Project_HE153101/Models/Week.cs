﻿using System;
using System.Collections.Generic;

namespace Final_Project_HE153101.Models
{
    public partial class Week
    {
        public Week()
        {
            ScheduleDetails = new HashSet<ScheduleDetail>();
        }

        public int WeekId { get; set; }
        public string WeekName { get; set; } = null!;

        public virtual ICollection<ScheduleDetail> ScheduleDetails { get; set; }
    }
}
