﻿using System;
using System.Collections.Generic;

namespace Final_Project_HE153101.Models
{
    public partial class ScheduleDetail
    {
        public int Id { get; set; }
        public int ScheduleId { get; set; }
        public int SlotId { get; set; }
        public int WeekId { get; set; }

        public virtual Schedule Schedule { get; set; } = null!;
        public virtual Slot Slot { get; set; } = null!;
        public virtual Week Week { get; set; } = null!;
    }
}
