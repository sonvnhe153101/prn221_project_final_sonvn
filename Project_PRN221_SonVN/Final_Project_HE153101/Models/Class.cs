﻿using System;
using System.Collections.Generic;

namespace Final_Project_HE153101.Models
{
    public partial class Class
    {
        public Class()
        {
            Schedules = new HashSet<Schedule>();
        }

        public int ClassId { get; set; }
        public string ClassName { get; set; } = null!;

        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}
