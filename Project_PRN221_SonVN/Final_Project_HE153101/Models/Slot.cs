﻿using System;
using System.Collections.Generic;

namespace Final_Project_HE153101.Models
{
    public partial class Slot
    {
        public Slot()
        {
            ScheduleDetails = new HashSet<ScheduleDetail>();
        }

        public int SlotId { get; set; }
        public string SlotName { get; set; } = null!;
        public DateTime StartSlot { get; set; }
        public DateTime EndSlot { get; set; }

        public virtual ICollection<ScheduleDetail> ScheduleDetails { get; set; }
    }
}
