﻿using System;
using System.Collections.Generic;

namespace Final_Project_HE153101.Models
{
    public partial class Schedule
    {
        public Schedule()
        {
            ScheduleDetails = new HashSet<ScheduleDetail>();
        }

        public int ScheduleId { get; set; }
        public string ScheduleName { get; set; } = null!;
        public int TeacherId { get; set; }
        public int RoomId { get; set; }
        public int ClassId { get; set; }
        public int SubjectId { get; set; }

        public virtual Class Class { get; set; } = null!;
        public virtual Room Room { get; set; } = null!;
        public virtual Subject Subject { get; set; } = null!;
        public virtual Teacher Teacher { get; set; } = null!;
        public virtual ICollection<ScheduleDetail> ScheduleDetails { get; set; }
    }
}
