﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Final_Project_HE153101.Models; 
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text.Json;

namespace Final_Project_HE153101.Pages.Schedules
{
    public class AddModel : PageModel
    {
        private readonly fapfptContext _context; 

        public AddModel(fapfptContext context)
        {
            _context = context;
        }
     
        [BindProperty]
        public Schedule Schedule { get; set; }

        public List<Class> Classes { get; set; }
        public List<Subject> Subjects { get; set; }
        public List<Teacher> Teachers { get; set; }
        public List<Room> Rooms { get; set; }
        public List<Slot> TimeSlots { get; set; }
        [BindProperty]
        public int SelectedTimeSlotId { get; set; }

        public int ScheduleId { get; set; }
        public virtual ICollection<ScheduleDetail> ScheduleDetails { get; set; }
        public async Task OnGetAsync()
        {
            Classes = await _context.Classes.ToListAsync();
            Subjects = await _context.Subjects.ToListAsync();
            Teachers = await _context.Teachers.ToListAsync();
            Rooms = await _context.Rooms.ToListAsync();
            TimeSlots = await _context.Slots.ToListAsync();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Schedules.Add(Schedule);
            await _context.SaveChangesAsync();

            var newScheduleDetail = new ScheduleDetail
            {
                ScheduleId = Schedule.ScheduleId,
                SlotId = SelectedTimeSlotId,
                                         
            };

            _context.ScheduleDetails.Add(newScheduleDetail);
            await _context.SaveChangesAsync();

            return RedirectToPage("./view");
        }


    }
}
