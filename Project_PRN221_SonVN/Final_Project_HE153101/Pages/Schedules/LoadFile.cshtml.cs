﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Final_Project_HE153101.Models;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Final_Project_HE153101.Pages.Schedules
{
    public class LoadFileModel : PageModel
    {
        private readonly fapfptContext _context;

        public LoadFileModel(fapfptContext context)
        {
            _context = context;
        }

        [BindProperty]
        public IFormFile Upload { get; set; }
        public class ScheduleInputModel
        {
            public string RoomName { get; set; }
            public string CourseCode { get; set; }
            public string ClassName { get; set; }
            public string TeacherName { get; set; }
            public string TimeslotTypeName { get; set; }
        }
        public List<ScheduleInputModel> SchedulesFromJson { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {
            if (Upload != null && Upload.Length > 0)
            {
                using (var reader = new StreamReader(Upload.OpenReadStream()))
                {
                    var fileContent = await reader.ReadToEndAsync();
                    var schedules = JsonConvert.DeserializeObject<List<ScheduleInputModel>>(fileContent);
                    SchedulesFromJson = JsonConvert.DeserializeObject<List<ScheduleInputModel>>(fileContent);


                    //foreach (var scheduleInput in schedules)
                    //{
                      
                    //    var room = await _context.Rooms.FirstOrDefaultAsync(r => r.RoomName == scheduleInput.RoomName);
                    //    if (room == null)
                    //    {
                            
                    //        room = new Room { RoomName = scheduleInput.RoomName };
                    //        _context.Rooms.Add(room);
                    //        await _context.SaveChangesAsync();
                    //    }

                      
                    //    var newSchedule = new Schedule
                    //    {
                        
                    //        RoomId = room.RoomId,
                          
                    //    };

                    //    _context.Schedules.Add(newSchedule);
                    //    await _context.SaveChangesAsync();

                     
                    //    var slot = await _context.Slots.FirstOrDefaultAsync(s => s.SlotName == scheduleInput.TimeslotTypeName);
                    //    if (slot != null)
                    //    {
                    //        var newScheduleDetail = new ScheduleDetail
                    //        {
                    //            ScheduleId = newSchedule.ScheduleId,
                    //            SlotId = slot.SlotId
                    //        };

                    //        _context.ScheduleDetails.Add(newScheduleDetail);
                    //        await _context.SaveChangesAsync();
                    //    }
                    //}

                }
                return Page();
            }
       
            ModelState.AddModelError(string.Empty, "Please upload a file.");
            return Page();
        }
    }
}
