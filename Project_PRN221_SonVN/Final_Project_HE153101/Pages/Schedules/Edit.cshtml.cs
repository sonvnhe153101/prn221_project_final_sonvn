﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Final_Project_HE153101.Models;
using System.Threading.Tasks;
using System.Linq;

namespace Final_Project_HE153101.Pages.Schedules
{
    public class EditModel : PageModel
    {
        private readonly fapfptContext _context;

        public EditModel(fapfptContext context)
        {
            _context = context;
            TimeSlots = new List<Slot>();
        }

        [BindProperty]
        public Schedule Schedule { get; set; }

        [BindProperty]
        public int SelectedSlotId { get; set; }
      


    
        public IList<Class> Classes { get; set; }
        public IList<Subject> Subjects { get; set; }
        public IList<Teacher> Teachers { get; set; }
        public IList<Room> Rooms { get; set; }
        public IList<Slot> TimeSlots { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Schedule = await _context.Schedules
                .Include(s => s.ScheduleDetails)
                .FirstOrDefaultAsync(m => m.ScheduleId == id);

            if (Schedule == null)
            {
                return NotFound();
            }

            SelectedSlotId = Schedule.ScheduleDetails.FirstOrDefault()?.SlotId ?? 0;

            Classes = await _context.Classes.ToListAsync();
            Subjects = await _context.Subjects.ToListAsync();
            Teachers = await _context.Teachers.ToListAsync();
            Rooms = await _context.Rooms.ToListAsync();
            TimeSlots = await _context.Slots.ToListAsync();

            if (Schedule == null)
            {
                return NotFound();
            }
            return Page();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Schedule).State = EntityState.Modified;

           
            var scheduleDetail = await _context.ScheduleDetails
                .FirstOrDefaultAsync(sd => sd.ScheduleId == Schedule.ScheduleId);

            if (scheduleDetail != null)
            {
                scheduleDetail.SlotId = SelectedSlotId; 
                _context.Attach(scheduleDetail).State = EntityState.Modified;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Schedules.Any(e => e.ScheduleId == Schedule.ScheduleId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./view");
        }


    }
}
