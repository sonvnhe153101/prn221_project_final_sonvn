﻿using Final_Project_HE153101.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_Project_HE153101.Pages.Schedules
{
    public class ViewModel : PageModel
    {
        private readonly fapfptContext _context;

        public ViewModel(fapfptContext context)
        {
            _context = context;
        }
        public string ClassFilter { get; set; }
        public string SubjectFilter { get; set; }
        public string TeacherFilter { get; set; }
        public string RoomFilter { get; set; }
        public string TimeSlotFilter { get; set; }
        public IList<Schedule> Schedules { get; set; } = default!;

        public IList<Class> Classes { get; set; } = new List<Class>();
        public IList<Subject> Subjects { get; set; } = new List<Subject>();
        public IList<Teacher> Teachers { get; set; } = new List<Teacher>();
        public IList<Room> Rooms { get; set; } = new List<Room>();
        public IList<Slot> TimeSlots { get; set; } = new List<Slot>();

        public int PageIndex { get; set; }
        public int TotalPages { get; set; }

        public bool HasPreviousPage => PageIndex > 1;
        public bool HasNextPage => PageIndex < TotalPages;

        public int PageSize { get; } = 4; 


        public async Task OnGetAsync(string classFilter, string subjectFilter, string teacherFilter,
            string roomFilter, int? timeSlotFilter, int pageIndex = 1)
        {
          
            ClassFilter = classFilter;
            SubjectFilter = subjectFilter;
            TeacherFilter = teacherFilter;
            RoomFilter = roomFilter;

            Classes = await _context.Classes.ToListAsync();
            Subjects = await _context.Subjects.ToListAsync();
            Teachers = await _context.Teachers.ToListAsync();
            Rooms = await _context.Rooms.ToListAsync();
            TimeSlots = await _context.Slots.ToListAsync();

            var query = _context.Schedules.AsQueryable();

            if (!string.IsNullOrEmpty(ClassFilter))
            {
                query = query.Where(s => s.Class.ClassName.Contains(ClassFilter));
            }
            if (!string.IsNullOrEmpty(SubjectFilter))
            {
                query = query.Where(s => s.Subject.SubjectName.Contains(SubjectFilter));
            }
            if (!string.IsNullOrEmpty(TeacherFilter))
            {
                query = query.Where(s => s.Teacher.TeacherName.Contains(TeacherFilter));
            }
            if (!string.IsNullOrEmpty(RoomFilter))
            {
                query = query.Where(s => s.Room.RoomName.Contains(RoomFilter));
            }
            int? timeSlotFilterId = null;
            if (int.TryParse(TimeSlotFilter, out var parsedId))
            {
                timeSlotFilterId = parsedId;
            }

            
            if (timeSlotFilterId.HasValue)
            {
                query = query.Where(s => s.ScheduleDetails.Any(sd => sd.SlotId == timeSlotFilterId.Value));
            }

            var totalItems = await query.CountAsync();
            TotalPages = (int)Math.Ceiling(totalItems / (double)PageSize);

            PageIndex = pageIndex;

            Schedules = await query
                .Skip((PageIndex - 1) * PageSize)
                .Take(PageSize)
                .Include(s => s.Class)
                .Include(s => s.Subject)
                .Include(s => s.Teacher)
                .Include(s => s.Room)
                .Include(s => s.ScheduleDetails).ThenInclude(sd => sd.Slot)
                .ToListAsync();

        }
        public async Task<IActionResult> OnPostDeleteAsync(int id)
        {
            try
            {
                var scheduledetail = _context.ScheduleDetails.Where(od => od.ScheduleId == id).ToList();

                if (scheduledetail.Any())
                {
                    _context.ScheduleDetails.RemoveRange(scheduledetail);
                    await _context.SaveChangesAsync();
                }

                var schedule = await _context.Schedules.FindAsync(id);
                if (schedule != null)
                {
                    _context.Schedules.Remove(schedule);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

                System.Diagnostics.Debug.WriteLine("Error deleting schedule: " + ex.Message);

            }

            return RedirectToPage();
        }

    }
}
